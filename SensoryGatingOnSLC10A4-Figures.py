#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@date: 20191218
@license: GNU GPLv3 <https://gitlab.com/malfatti/SensoryGatingOnSLC10A4/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/SensoryGatingOnSLC10A4

Slc10a4 knockout mice show abnormal auditory gating of N40 and P80 event-related potentials
"""

#%% Load dependencies ==========================================================
ScriptName = 'SensoryGatingOnSLC10a4'

print(f'[{ScriptName}] Loading dependencies...')
import numpy as np, os
from copy import deepcopy as dcp
from glob import glob
from sciscripts.Analysis import Analysis, ERPs, Stats
from sciscripts.IO import IO

from sciscripts.Analysis.Plot import Plot
from sciscripts.Analysis.Plot import Stats as PlotStats
from svgutils import compose as svgc
from svgutils import transform as svgt

plt = Plot.Return('plt')
plt.style.use('SciScripts_Small')

ListedColormap = Plot.Return('ListedColormap')

DataPath = os.environ['DATAPATH']+'/SensoryGatingOnSLC10a4'
AnalysisPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Analysis/SensoryGatingOnSLC10a4'
FiguresPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Manuscripts/Ciralli_20210422'
SourcesPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Manuscripts/Ciralli_20210422/Sources'

GroupsOrder = ('WT', 'KO')
GroupsNames = ('WT', '$Slc10a4^{-/-}$')
GroupsNamesTex = (
    '\\textbf{'+GroupsNames[0]+'}',
    '\\textit{\\boldmath'+GroupsNames[1]+'}'
)
ClicksOrder = ('1stClick', '2ndClick')
GroupsBreak = ('','')
TreatmentsOrder = ('NaCl', 'Ket5mg', 'Ket20mg')
TreatmentsNames = ('NaCl', 'Ket 5mg/kg', 'Ket 20mg/kg')
TrialToRun = 0

Save = False
Margins = [50, 50]
A4Size = Plot.FigSizeA4.copy()
A4Size = [Size-Margins[S]*0.03937008 for S,Size in enumerate(A4Size)]
Ext = ['svg']
SPLA = {'size':9.5, 'va':'bottom', 'ha': 'left', 'weight':'bold'}
SigA = {'size':6}
LA = dict(handletextpad=0.35, borderpad=0.1, borderaxespad=0.1)

Markers = {
    'WT': '^',
    'KO': 's',
}
Colors = {
    '1stClick': [0, 0, 0],
    '2ndClick': [204/255, 0, 0],
    'WT': [0, 120/255, 0],
    'KO': [0, 0, 120/255],
    'N40': [0, 1, 0],
    'P80': [0, 0, 1],
    'Arrows': 'c',
    'N40CMap': 'inferno',
    'P80CMap': 'inferno'
}
Colors = {K: np.array(V) for K,V in Colors.items()}


def AnovaStars(Anova, Ax, TicksDir='down', LineTextSpacing=1):
    ak = Stats.spk(Anova['PWCs']['Genotype'])
    AnovaInd = Anova['PWCs']['Genotype'][ak]['p.adj'] < 0.05
    if True in AnovaInd:
        tr = Anova['PWCs']['Genotype'][ak]['Treatment'][AnovaInd]
        ps = Anova['PWCs']['Genotype'][ak]['p.adj'][AnovaInd]
        g1 = Anova['PWCs']['Genotype'][ak]['group1'][AnovaInd]
        g2 = Anova['PWCs']['Genotype'][ak]['group2'][AnovaInd]

        for i,p in enumerate(ps):
            x = TreatmentsOrder.index(tr[i])*3
            x = [x, x+1]
            Y = max(Ax.get_ylim())
            p = '{:.1e}'.format(p)
            Plot.SignificanceBar(
                x, [Y*1.05]*2, p, Ax, TextArgs=SigA,
                TicksDir=TicksDir, LineTextSpacing=LineTextSpacing
            )

    ak = Stats.spk(Anova['PWCs']['Treatment'])
    AnovaInd = Anova['PWCs']['Treatment'][ak]['p.adj'] < 0.05
    if True in AnovaInd:
        f1 = np.array([
            _ for a in [
                [b]*(
                    len(Anova['PWCs']['Treatment'][ak]['group1'])
                    //len(Anova['PWCs']['Treatment'][ak]['Genotype'])
                )
                for b in Anova['PWCs']['Treatment'][ak]['Genotype']
            ] for _ in a
        ])
        tr = f1[AnovaInd]
        ps = Anova['PWCs']['Treatment'][ak]['p.adj'][AnovaInd]
        g1 = Anova['PWCs']['Treatment'][ak]['group1'][AnovaInd]
        g2 = Anova['PWCs']['Treatment'][ak]['group2'][AnovaInd]

        for i,p in enumerate(ps):
            x = [TreatmentsOrder.index(_)*3+1 for _ in [g1[i], g2[i]]]
            y = max(Ax.get_ylim())
            p = '{:.1e}'.format(p)
            Plot.SignificanceBar(
                x, [y*1.05]*2, p, Ax, TextArgs=SigA,
                TicksDir=TicksDir, LineTextSpacing=LineTextSpacing
            )

    return(Ax)


print(f'[{ScriptName}] Done.')



#%% [Locomotion] Fig Mov =======================================================
FigName = f'CiralliEtAl_FigMovDist'
QntBy = 'Group'
SetLabels = dict(
    DistSum = 'Total dist. [cm]',
    DistMean = 'Avg. dist. [cm]',
    Area = 'Area [cm$^{2}$]',
    SpeedMean = 'Avg. speed [cm/s]'
)

Animals, Treatments, Genotypes, Trials = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/Trackings/{_}.dat')[0]
    for _ in ('Animals', 'Treatments', 'Genotypes', 'Trials')
)

AnimalsToIgnore = np.ones(Animals.shape, dtype=bool)
AnimalsToIgnore[(Animals=='36537')] = False

Sets = {
    _: IO.Bin.Read(f'{AnalysisPath}/GroupData/Trackings/{_}.dat')[0]
    for _ in ('DistMean',)
    # for _ in ('DistSum', 'DistMean', 'SpeedMean', 'Area')
}

Coords = {
    _: IO.Bin.Read(f'{AnalysisPath}/GroupData/Trackings/{_}')[0]
    for _ in ('X','Y')
}

if type(TrialToRun) == int and TrialToRun > 0:
    AnimalsU = [
        Animal for Animal in np.unique(Animals)
        if max(Trials[Animals==Animal]) < TrialToRun
    ]
    for Animal in AnimalsU:
        TrialMax = max(Trials[Animals==Animal])
        Trials[(Trials==TrialMax)*(Animals==Animal)] = TrialToRun

if type(TrialToRun) == int:
    t = Trials == TrialToRun
    t = t*AnimalsToIgnore

    Animals, Genotypes, Treatments = (_[t] for _ in (Animals, Genotypes, Treatments))
    Coords = {K:[El for E,El in enumerate(V) if t[E]] for K,V in Coords.items()}
    Sets = {K: V[t] for K,V in Sets.items()}

ExpInfo = IO.Txt.Read(f'{AnalysisPath}/GroupData/GroupInfo/ExpInfo.dict')
PixelSize = ExpInfo['Video']['PixelSize']
IMGSize = ExpInfo['Video']['IMGSize']

AnimalsU, i = np.unique(Animals, return_index=True)
GenN = [np.where(Genotypes[i]==_)[0].shape[0] for _ in GroupsOrder]

FigSize = [A4Size[0], A4Size[1]*0.7]
Fig = plt.figure(figsize=FigSize)

GS = Fig.add_gridspec(3,1, height_ratios=(0.15,0.45,0.45), left=0.10, right=0.95, bottom=0.04)
GSGr = GS[1].subgridspec(1,len(GroupsOrder), wspace=0.25)
GSQnt = GS[2].subgridspec(1, len(Sets))
GSTr = [
    GSGr[g].subgridspec(G,len(TreatmentsOrder), wspace=0, hspace=0)
    for g,G in enumerate(GenN)
]

AxesTr = [
    np.array([
        [Fig.add_subplot(G[l,c]) for c in range(len(TreatmentsOrder))]
        for l in range(GenN[g])
    ])
    for g,G in enumerate(GSTr)
]
AxesQnt = np.array([Fig.add_subplot(_) for _ in GSQnt])

for G,Group in enumerate(GroupsOrder):
    GAnimals = AnimalsU[Genotypes[i]==Group]

    for A, Animal in enumerate(GAnimals):
        for Tr,Treatment in enumerate(TreatmentsOrder):
            xl, yl = [_*PixelSize for _ in IMGSize]
            AxesTr[G][A,Tr].set_xlim([0, xl])
            AxesTr[G][A,Tr].set_ylim([0, yl])

            GATr = (Animals==Animal)*(Treatments==Treatment)

            if True not in GATr:
                nfa = {'size':6, 'va':'center', 'ha':'center'}
                AxesTr[G][A,Tr].text(xl/2, yl/2, 'Not found', nfa)
                continue

            GATr = np.where(GATr)[0]
            if GATr.shape[0] > 1:
                raise ValueError('There should be only one rec for each animal at each treatment.')

            GATr = GATr[0]
            GATrX, GATrY = (Coords[_][GATr] for _ in ('X','Y'))

            AxesTr[G][A,Tr].plot(GATrX, GATrY, 'k.--', ms=1, lw=0.8)
            if not A: AxesTr[G][A,Tr].set_title(TreatmentsNames[Tr])

        AxesTr[G][A,0].set_ylabel(Animal, size=6, labelpad=0)

    for AxL in AxesTr[G]:
        for Ax in AxL:
            for Side in ['right', 'left', 'bottom', 'top']:
                Ax.spines[Side].set_visible(True)
                Ax.spines[Side].set_linewidth(1)
            Ax.set_xticks([])
            Ax.set_yticks([])
            Ax.set_xticklabels('')
            Ax.set_yticklabels('')

    Pos = np.array([_.get_position().corners() for _ in AxesTr[G][:,0]]).T
    PosMin = np.array([Pos[:,0,_] for _ in range(Pos.shape[2])])
    PosMax = np.array([Pos[:,1,_] for _ in range(Pos.shape[2])])
    PosRng = PosMax[:,1].max()-PosMin[:,1].min()
    Xt = PosMin[0,0]-0.05
    Yt = PosMin[:,1].min()+(PosRng/2)
    fd = dict(va='center',ha='center',rotation=90,size=plt.rcParams['axes.labelsize'])
    Fig.text(Xt, Yt, 'Mouse ID',fd)


# Quants
XQG = [
    [t+((len(TreatmentsOrder)+1)*g) for t in range(len(TreatmentsOrder))]
    for g in range(len(GroupsOrder))
]
XQTr = [
    [g+((len(GroupsOrder)+1)*t) for g in range(len(GroupsOrder))]
    for t in range(len(TreatmentsOrder))
]
if QntBy=='Group':
    XQ = dcp(XQG)
    XTLs = list(TreatmentsNames*len(GroupsOrder))
else:
    XQ = dcp(XQTr)
    XTLs = list(GroupsOrder*len(TreatmentsOrder))

for S,(SL,Set) in enumerate(Sets.items()):
    Anova = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/Behavior-{SL}-Genotype_Treatment-Trial{TrialToRun}.dict')
    Anova = IO.Txt.DictListsToArrays(Anova)

    # Mean+SEM
    if QntBy=='Group':
        Y = [
            [Set[(Genotypes==G)*(Treatments==Tr)] for Tr in TreatmentsOrder]
            for G in GroupsOrder
        ]

        for y in range(len(GroupsOrder)):
            Plot.ScatterMean(Y[y], XQ[y],
                Paired=True, CMap='tab10',
                Ax=AxesQnt[S]
            )
    else:
        Y = [
            [Set[(Genotypes==G)*(Treatments==Tr)] for G in GroupsOrder]
            for Tr in TreatmentsOrder
        ]

        for y in range(len(TreatmentsOrder)):
            Plot.ScatterMean(Y[y], XQ[y],
                Paired=False, CMap='tab10',
                Ax=AxesQnt[S]
            )


    # Statistics
    px, py, pt = [], [], []
    yr = np.ptp(AxesQnt[S].get_ylim())
    step = yr*0.08

    for G,Group in enumerate(GroupsOrder):
        AR = Anova['PWCs']['Treatment']
        AR = AR[Stats.spk(AR)]
        pi = AR['Genotype']==Group
        ps = AR['p.adj'][pi]
        g1, g2 = (AR[f'group{_}'][pi] for _ in ('1','2'))

        if QntBy=='Group':
            x = [[XQ[G][TreatmentsOrder.index(_[g])] for _ in (g1,g2)] for g in range(len(g1))]
        else:
            x = [[XQ[TreatmentsOrder.index(_[g])][G] for _ in (g1,g2)] for g in range(len(g1))]

        y = max(AxesQnt[S].get_ylim())

        for pp,p in enumerate(ps):
            if p < 0.05:
                p = '{:.1e}'.format(p)
            else:
                p = 'n.s.'

            px.append(x[pp])
            py.append([y+step*pp]*2)
            pt.append(p)

    po = np.ptp(px,axis=1).argsort()
    for p in po:
        y = max(AxesQnt[S].get_ylim())
        ppy = py[p] if QntBy=='Group' else [y+step*p]*2

        Plot.SignificanceBar(
            px[p], ppy, pt[p],
            AxesQnt[S], TextArgs=SigA, TicksDir=None,
            LineTextSpacing=1.03
        )

    px, py, pt = [], [], []
    yr = np.ptp(AxesQnt[S].get_ylim())
    step = yr*0.02

    for Tr,Treatment in enumerate(TreatmentsOrder):
        AR = Anova['PWCs']['Genotype']
        AR = AR[Stats.spk(AR)]
        pi = AR['Treatment']==Treatment
        ps = AR['p.adj'][pi]
        g1, g2 = (AR[f'group{_}'][pi] for _ in ('1','2'))

        if QntBy=='Group':
            x = [[XQ[GroupsOrder.index(_[g])][Tr] for _ in (g1,g2)] for g in range(len(g1))]
        else:
            x = [[XQ[Tr][GroupsOrder.index(_[g])] for _ in (g1,g2)] for g in range(len(g1))]

        y = max(AxesQnt[S].get_ylim())

        for pp,p in enumerate(ps):
            if p < 0.05:
                p = '{:.1e}'.format(p)
            else:
                p = 'n.s.'

            px.append(x[pp])
            py.append([y+step*pp]*2)
            pt.append(p)

    po = np.ptp(px,axis=1).argsort()
    for p in po:
        y = max(AxesQnt[S].get_ylim())
        ppy = [y+step*(p+1)]*2

        Plot.SignificanceBar(
            px[p], ppy, pt[p],
            AxesQnt[S], TextArgs=SigA, TicksDir=None,
            LineTextSpacing=1.03
        )

    for Tr,Treatment in enumerate(TreatmentsOrder):
        AR = Anova['PWCs']['Genotype']
        AR = AR[Stats.spk(AR)]
        pi = AR['Treatment']==Treatment
        ps = AR['p.adj'][pi]

        for pp,p in enumerate(ps):
            if p < 0.05:
                x = [XQG[0][Tr], XQG[1][Tr]]
                y = max(AxesQnt[S].get_ylim())
                p = '{:.1e}'.format(p)
                Plot.SignificanceBar(
                    x, [y]*2, p,
                    AxesQnt[S], TextArgs=SigA, TicksDir=None,
                    LineTextSpacing=1.06
                )

    YL = max(AxesQnt[S].get_ylim())*1.05
    YR = abs(np.diff(AxesQnt[S].get_ylim()))[0]
    YT = max((YL+(YR*0.1) for _ in range(len(GroupsOrder))))

    if QntBy=='Group':
        for G,Group in enumerate(GroupsOrder):
            SqX = (XQ[G][0], XQ[G][0], XQ[G][-1], XQ[G][-1], XQ[G][0])
            SqY = (YL, YT, YT, YL, YL)
            SqXC, SqYC = (np.mean((min(_), max(_))) for _ in (SqX,SqY))
            AxesQnt[S].plot(SqX, SqY, 'k')
            AxesQnt[S].text(
                SqXC, SqYC*0.99, GroupsNamesTex[G],
                va='center',ha='center',weight='bold',
                usetex=True,math_fontfamily='dejavusans'
            )
    else:
        ed = 0.07
        for Tr,Treatment in enumerate(TreatmentsOrder):
            SqX = (XQ[Tr][0]*(1-ed), XQ[Tr][0]*(1-ed), XQ[Tr][-1]*(1+ed), XQ[Tr][-1]*(1+ed), XQ[Tr][0]*(1-ed))
            SqY = (YL, YT, YT, YL, YL)
            SqXC, SqYC = (np.mean((min(_), max(_))) for _ in (SqX,SqY))
            AxesQnt[S].plot(SqX, SqY, 'k')
            AxesQnt[S].text(
                SqXC, SqYC*0.99, Treatment,
                dict(va='center',ha='center',weight='bold')
            )


    AxesQnt[S].set_ylabel(SetLabels[SL])
    Plot.Set(Ax=AxesQnt[S])
    AxesQnt[S].set_xticks(np.ravel(XQ))
    Rot = 360-45 if QntBy=='Group' else 0
    AxesQnt[S].set_xticklabels(XTLs)#, rotation=Rot)


Pos = np.array([P.get_position().corners() for P in (_ for Ax in AxesTr for _ in Ax[0,:])]).T
Pos = [
    [
        Pos[0,1,_],
        Pos[1,1,_]*1.04,
        (Pos[0,2,_]-Pos[0,1,_]),
        (Pos[1,1,_]-Pos[1,0,_])*0.4
    ]
    for _ in range(Pos.shape[2])
]

pax = (0,3)
for _ in pax:
    Pos[_][2] = (Pos[_+2][0]+Pos[_+2][2])-Pos[_][0]

AxRects = [Fig.add_axes(Pos[_], zorder=-1) for _ in pax]
for A,Ax in enumerate(AxRects):
    Ax.add_patch(plt.Rectangle((0,0),1,1, facecolor='w', edgecolor='k', clip_on=False, linewidth=1))
    Ax.text(
        0.5, 0.4, GroupsNamesTex[A],
        ha='center', va='center', weight='bold',
        usetex=True, math_fontfamily='dejavusans'
    )
    Ax.axis('off')


# Add sublabels
Pos = np.array([_.get_position().corners() for _ in (AxesTr[0][0,0], AxesTr[1][0,0], AxesQnt[0])]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0, 0.6, Pos[1][0]-0.05]
Y = [0.96, Pos[1][1]+0.03, Pos[2][1]]
AxesPos = [
    [X[0], Y[0]], [X[1], Y[0]],
    [X[0], Y[1]], [X[2], Y[1]],
    [X[0], Y[2]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write fig
if Save:
    Fig.savefig(f'{SourcesPath}/{FigName}-NoIMG.svg', transparent=True)
    FigSize_pt = Plot.InchToPoint(np.array(FigSize))
    svgc.Figure(str(FigSize_pt[0])+'pt', str(FigSize_pt[1])+'pt',
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-IMG.svg')),
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-NoIMG.svg'))
    ).save(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% [ERPs] Fig Hist Chs Avg ====================================================
FigName = f'CiralliEtAl_FigHistChsAvg'

ExAnimal, ExTreat = '40162', 'NaCl'
ScaleBar = 500

KeyData = 'DataChannels'
KeyPeak = 'N40'
PeakLabels = ['1st click', '2nd click']


# Load data
Animals, Genotypes, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/GroupInfo/{_}.dat')[0]
    for _ in ('Animals', 'Genotypes', 'Treatments', 'Trials')
)

if type(TrialToRun) == int and TrialToRun > 0:
    AnimalsU = [
        Animal for Animal in np.unique(Animals)
        if max(Trials[Animals==Animal]) < TrialToRun
    ]
    for Animal in AnimalsU:
        TrialMax = max(Trials[Animals==Animal])
        Trials[(Trials==TrialMax)*(Animals==Animal)] = TrialToRun

ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
ERPsX = ERPsTraces['X']

ExpInfo = IO.Txt.Read(f'{AnalysisPath}/GroupData/GroupInfo/ExpInfo.dict')
ExpInfo['EPhys']['BrokenChs'] = np.array(ExpInfo['EPhys']['BrokenChs'], dtype=object)
ExpInfo = IO.Txt.DictListsToArrays(ExpInfo)


if type(TrialToRun) == int:
    t = Trials == TrialToRun

    Animals, Genotypes, Treatments = (_[t] for _ in (Animals, Genotypes, Treatments))

    for K in ('Channels', 'Rates'): ERPsTraces[K] = ERPsTraces[K][t]
    for K in ('DataChannels', 'DataChannelsMean'): ERPsTraces[K] = ERPsTraces[K][:,t]
    ERPsTraces['ERPMeans'] = ERPsTraces['ERPMeans'][:,:,t]


AnimalTraces = (Animals == ExAnimal) * (Treatments == ExTreat)
AnimalChannel = ERPsTraces['Channels'][np.where(AnimalTraces)[0][0]]
AnimalBrokenCh = ExpInfo['EPhys']['BrokenChs'][ExpInfo['Animals']==ExAnimal][0]
ERPMean = ERPsTraces['ERPMeans'][:,:,np.where(AnimalTraces)[0][0]]
YLim = [ERPMean[:,AnimalChannel-1].min(), ERPMean[:,AnimalChannel-1].max()]


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.5]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(2,1)
GSTop = GS[0].subgridspec(1,2)
GSBottom = GS[1].subgridspec(2,3,height_ratios=(0.3,0.7))
AxHist = Fig.add_subplot(GSTop[0])
AxesTraces = np.array([
    Fig.add_subplot(GSTop[1]),
    Fig.add_subplot(GSBottom[:,0]),
    Fig.add_subplot(GSBottom[1,1]),
    Fig.add_subplot(GSBottom[1,2]),
])
AxTL = Fig.add_subplot(GSBottom[0,1:])


# Histology
TA = {'va':'bottom','ha':'left', 'color':'w'}
AxHist.axis('off')
AxHist.set_xlim((0,1))
AxHist.set_ylim((0,1))
AxHist.text(0.74, 0.4, 'dHipp', TA)

TA = {'va':'top','ha':'center', 'color':'w', 'size':6}
AA = dict(arrowstyle="->, head_width=0.3", lw=1.5, shrinkA=0, color='w')
ALen = 0.16
Axy = (0.84, 0.00)
AxHist.annotate('', xy=(Axy[0]-ALen, Axy[1]), xytext=Axy, arrowprops=AA)
AxHist.annotate('', xy=(Axy[0], Axy[1]+ALen), xytext=Axy, arrowprops=AA)
AxHist.text((Axy[0]-ALen/2)*1.03, Axy[1]-0.03, 'Lateral', TA)

TA = {'va':'center','ha':'center', 'color':'w', 'size':6}
AxHist.text(Axy[0]*1.05, Axy[1]+ALen/2, 'Dorsal', TA, rotation=90)


# All ch
ChGood = np.array([_ for _ in np.arange(ERPMean.shape[1])+1 if _ not in AnimalBrokenCh])
AxesTraces[0] = Plot.AllCh(
    ERPMean[:,ChGood-1], ERPsX, Labels=ChGood,
    ScaleBar=ScaleBar, SpaceAmpF=1, lw=1,
    Ax=AxesTraces[0], AxArgs={'xticks': np.linspace(ERPsX[0],ERPsX[-1],4)}
)

SqCh = np.where((ChGood==AnimalChannel))[0][0]+1
SquareY = AxesTraces[0].get_lines()[SqCh].get_ydata()
SquareY = [SquareY.min(), SquareY.max(), SquareY.max(), SquareY.min(), SquareY.min()]
SquareX = [ERPsX[0], ERPsX[0], ERPsX[-1], ERPsX[-1], ERPsX[0]]
AxesTraces[0].plot(SquareX, SquareY, color='silver', linestyle='--')

Scale = AxesTraces[0].get_lines()[0].get_xydata()
Scale[:,1] -= 1500
AxesTraces[0].get_lines()[0].set_xdata(Scale[:,0])
AxesTraces[0].get_lines()[0].set_ydata(Scale[:,1])
AxesTraces[0].text(Scale[0,0]-0.05, np.mean(Scale[:,1]), f'{ScaleBar}µV', rotation=90, ha='center', va='center', fontsize=8)

AxesTraces[0].set_xlabel('Time [s]')
AxesTraces[0].set_ylabel('Channels')
Plot.Set(Ax=AxesTraces[0])


# Chosen ch
AxesTraces[1].plot([ERPsX[ERPsX>=0.04][0]*1000]*2, YLim, 'k--', lw=0.7)
AxesTraces[1].plot([ERPsX[ERPsX>=0.08][0]*1000]*2, YLim, 'k--', lw=0.7)

AxesTraces[1].plot(
    ERPsX[(0.2>ERPsX)*(ERPsX>=-0.1)]*1000,
    ERPMean[(0.2>ERPsX)*(ERPsX>=-0.1),AnimalChannel-1],
    'k', lw=1, label='Click 1'
)
AxesTraces[1].plot(
    ERPsX[(0.2>ERPsX)*(ERPsX>=-0.1)]*1000,
    ERPMean[(0.7>ERPsX)*(ERPsX>=0.4),AnimalChannel-1],
    'r', lw=1, label='Click 2'
)

AxesTraces[1].set_title(f'Channel {AnimalChannel}', pad=-8)
AxesTraces[1].set_xlabel('Time [ms]')
AxesTraces[1].set_ylabel('Voltage [µv]')
AxesTraces[1].legend(loc='lower left', handlelength=0.6, bbox_to_anchor=(-0.00,0,1,1), **LA)
Plot.Set(Ax=AxesTraces[1])

AxesTraces[1].spines['bottom'].set_visible(False)
AxesTraces[1].set_xticks([])
for T in zip([40,80], [YLim[0]+YLim[0]*0.1]*2, ['40', '80']):
    AxesTraces[1].text(T[0], T[1], T[2], ha='center', va='top', fontsize=8)

AxesTraces[1].xaxis.labelpad = 20


# Timeline
TA = {'size':8, 'va':'bottom','ha':'center'}
for t in Analysis.MovingAverage(range(len(TreatmentsOrder))):
    AxTL.text(t, 0.05, '10 min.', TA)

AxTL.set_xlim((-1.5,len(TreatmentsOrder)-1))
AxTL.set_xticks(range(len(TreatmentsOrder)))
AxTL.set_xticklabels(TreatmentsNames)
AxTL.set_ylim((0,1))
AxTL.set_yticks(())
AxTL.spines['left'].set_visible(False)
Plot.Set(Ax=AxTL)


# Avg traces
XLim = [round(ERPsX[0], 3), round(ERPsX[-1], 3)]
ColorsT = [Colors['WT'], Colors['KO']*2]
for C, Cond in enumerate([Genotypes=='WT', Genotypes=='KO']):
    Ind = C+2
    CondTreat = Cond
    Mean = ERPsTraces[KeyData][:,CondTreat].mean(axis=1)

    SLim = [
        ERPsTraces[KeyData][:,CondTreat].min(),
        ERPsTraces[KeyData][:,CondTreat].max()
    ]

    AxesTraces[Ind].plot([0,0], SLim, color=Colors['1stClick'], linestyle='--')
    AxesTraces[Ind].plot([0.5,0.5], SLim, color=Colors['2ndClick'], linestyle='--')
    AxesTraces[Ind].plot(ERPsX, ERPsTraces[KeyData][:,CondTreat], 'silver', lw=0.7)
    AxesTraces[Ind].plot(ERPsX, Mean, color=ColorsT[C], lw=1)

    PFun = np.argmin if KeyPeak == 'N40' else np.argmax
    PeaksX = [
        PFun(Mean[P])+(np.where((ERPsX>=0.5))[0][0]*p)
        for p,P in enumerate((ERPsX<0.1,ERPsX>=0.5))
    ]

    AxesTraces[Ind].plot(ERPsX[PeaksX], Mean[PeaksX], 'm--')
    AxesTraces[Ind].text(
        np.mean(ERPsX[PeaksX]), min(Mean[PeaksX]), 'N40',
        dict(color='m', va='top', ha='center')
    )

    AxArgs = {
        'xlim': XLim,
        'xticks': [-0.5, 0, 0.5, 1],
        'xlabel': 'Time [s]',
        'title': GroupsNames[C]
    }
    Plot.Set(Ax=AxesTraces[Ind], AxArgs=AxArgs)

    AxesTraces[Ind].yaxis.set_visible(False)
    AxesTraces[Ind].spines['left'].set_visible(False)


# Set fig
Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add sublabels
Pos = np.array([_.get_position().corners() for _ in AxesTraces]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0, Pos[0][0]-0.06, Pos[2][0]-0.03]
Y = [0.96, Pos[1][1]]
AxesPos = [
    [X[0], Y[0]], [X[1], Y[0]],
    [X[0], Y[1]], [X[2], Y[1]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)

YTLs = [
    E for E,El in enumerate(AxesTraces[0].get_yticklabels())
    if El.get_text() not in ('15','12','14')
]
AxesTraces[0].set_yticks(np.array(AxesTraces[0].get_yticks())[YTLs])


# Write fig
if Save:
    Fig.savefig(f'{SourcesPath}/{FigName}-NoIMG.svg', transparent=True)
    FigSize_pt = Plot.InchToPoint(np.array(FigSize))
    svgc.Figure(str(FigSize_pt[0])+'pt', str(FigSize_pt[1])+'pt',
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-IMG.svg')),
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-NoIMG.svg'))
    ).save(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% [ERPs] Figure Avg Amp Ratio {KeyPeak} ======================================
KeyPeak = 'N40'
FigName = f'CiralliEtAl_FigAvgAmpRatio{KeyPeak}'

KeyData = 'DataChannels'
PeakLabels = ['1st click', '2nd click']
PeakMarkers = ['s', '^']
PlotType = ['scatter']
RatiosLabels = ['2nd peak suppr. [%]', '2nd peak delay [%]']
SMArgs = dict(Alpha=0.4, ErrorArgs=dict(capsize=3))


# Load data
Animals, Genotypes, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/GroupInfo/{_}.dat')[0]
    for _ in ('Animals', 'Genotypes', 'Treatments', 'Trials')
)

KeyPeaks, PeakAmps, PeakLats = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeaks/{_}.dat')[0]
    for _ in ('PeaksOrder', 'PeakAmps', 'PeakLats')
)

PeakAmpRatios, PeakLatRatios = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeaks/{K}.dat')[0]
    for K in ('PeakAmpRatios', 'PeakLatRatios')
)


if type(TrialToRun) == int and TrialToRun > 0:
    AnimalsU = [
        Animal for Animal in np.unique(Animals)
        if max(Trials[Animals==Animal]) < TrialToRun
    ]
    for Animal in AnimalsU:
        TrialMax = max(Trials[Animals==Animal])
        Trials[(Trials==TrialMax)*(Animals==Animal)] = TrialToRun


ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
ERPsX = ERPsTraces['X']
KeyInd = KeyPeaks.tolist().index(KeyPeak)
PeakAmps = abs(PeakAmps)


if type(TrialToRun) == int:
    t = Trials == TrialToRun

    Animals, Genotypes, Treatments = (_[t] for _ in (Animals, Genotypes, Treatments))
    PeakAmps, PeakLats = (_[:,t,:] for _ in (PeakAmps, PeakLats))
    PeakAmpRatios, PeakLatRatios = (_[t,:] for _ in (PeakAmpRatios, PeakLatRatios))

    for K in ('Channels', 'Rates'): ERPsTraces[K] = ERPsTraces[K][t]
    for K in ('DataChannels', 'DataChannelsMean'): ERPsTraces[K] = ERPsTraces[K][:,t]
    ERPsTraces['ERPMeans'] = ERPsTraces['ERPMeans'][:,:,t]


PWindows = [
    [np.where((ERPsX>=P))[0][0] for P in W]
    for W in ((0,0.12), (0.5,0.62))
]


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.7]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(4,1)
GSTraces = GS[0].subgridspec(2,len(TreatmentsOrder)+1,width_ratios=(0.2,)+(0.8,)*len(TreatmentsOrder))
GSQnt = GS[1:3].subgridspec(2,2)
GSRatios = GS[3].subgridspec(1,2)#,width_ratios=(2/3,1/3))

AxesTraces = np.array([
    [Fig.add_subplot(GSTraces[G,T+1]) for T in range(len(TreatmentsOrder))]
    for G in range(len(GroupsOrder))
])

AxesQnt = np.array([
    Fig.add_subplot(GSQnt[0,0]),
    Fig.add_subplot(GSQnt[0,1]),
    Fig.add_subplot(GSQnt[1,0]),
    Fig.add_subplot(GSQnt[1,1]),
])

AxesRatios = np.array([
    Fig.add_subplot(GSRatios[0]),
    Fig.add_subplot(GSRatios[1]),
])


# Traces
XLim = [round(ERPsX[0], 3), round(ERPsX[-1], 3)]
ColorsC = [
    Colors['WT'].copy(),
    Colors['KO'].copy()
]

if KeyPeak == 'N40':
    ColorsC[1] *= 2
elif KeyPeak == 'P80':
    ColorsC[1] = [150/255, 0, 170/255]

for C, Cond in enumerate([Genotypes=='WT', Genotypes=='KO']):
    for T, Treatment in enumerate(TreatmentsOrder):
        CondTreat = Cond * (Treatments == Treatment)
        Mean = ERPsTraces[KeyData][:,CondTreat].mean(axis=1)

        SLim = [
            ERPsTraces[KeyData][:,CondTreat].min(),
            ERPsTraces[KeyData][:,CondTreat].max()
        ]
        AxesTraces[C,T].plot([0,0], SLim, color=Colors['1stClick'], linestyle='--', lw=1)
        AxesTraces[C,T].plot([0.5,0.5], SLim, color=Colors['2ndClick'], linestyle='--', lw=1)
        AxesTraces[C,T].plot(ERPsX, ERPsTraces[KeyData][:,CondTreat], 'silver', lw=0.7)
        AxesTraces[C,T].plot(ERPsX, Mean, color=ColorsC[C], lw=1)

        AxArgs = {'xlim': XLim, 'xticks': [-0.5, 0, 0.5, 1]}
        Plot.Set(Ax=AxesTraces[C,T], AxArgs=AxArgs)

        if not C: AxesTraces[C,T].axis('off')
        else:
            AxesTraces[C,T].yaxis.set_visible(False)
            AxesTraces[C,T].spines['left'].set_visible(False)

        if not (C+T):
            TA = dict(size=7, va='bottom', ha='center')
            AxesTraces[C,T].text(0, SLim[1], '1', color=Colors['1stClick'], **TA)
            AxesTraces[C,T].text(0.5, SLim[1], '2', color=Colors['2ndClick'], **TA)

            PeaksX = ERPs.GetComponents(Mean, ERPsX, PWindows)
            PeaksX = {K:V for K,V in zip(KeyPeaks,PeaksX[:-1])}

            VA = 'top' if 'N' in KeyPeak else 'bottom'
            AxesTraces[C,T].plot(ERPsX[PeaksX[KeyPeak]], Mean[PeaksX[KeyPeak]], 'm--')
            AxesTraces[C,T].text(
                np.mean(ERPsX[PeaksX[KeyPeak]]), min(Mean[PeaksX[KeyPeak]]), KeyPeak,
                dict(color='m', va=VA, ha='center', size=7)
            )

for A,Ax in enumerate(AxesTraces[0,:]): Ax.set_title(TreatmentsNames[A])
for Ax in AxesTraces[-1,:]: Ax.set_xlabel('Time [s]')


# Amps and Lats
PeakAmps = abs(PeakAmps)
ColorsCc = [Colors['1stClick'], Colors['2ndClick']]

XPos = [
    np.arange(
        0,
        len(TreatmentsOrder)*(len(GroupsOrder)+1),
        len(GroupsOrder)+1
    )+_
    for _ in range(2)
]
YPos = [[i*3,i*3+1] for i in range(len(TreatmentsOrder))]
XTicks = [np.mean(_) for _ in YPos]
SigSpace = [[1.15, 1.05], [1.05, 1.05]]

AL = ('Amp', 'Lat')
for V,Var in enumerate([PeakAmps[:,:,KeyInd], PeakLats[:,:,KeyInd]]):

    v = AL[V]
    Anova = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/ERPs-{v}-Click_Genotype_Treatment-{KeyPeak}-Trial{TrialToRun}.dict')
    Anova = IO.Txt.DictListsToArrays(Anova)

    z = Stats.sst.zscore(Var.ravel())
    z[(z<-3)+(z>3)] = np.nan
    z[~np.isnan(z)] = 1
    z = z.reshape(Var.shape)
    z = np.hstack([[np.prod(z,axis=0)]*2])

    Var = Var*z

    for G,Group in enumerate((Genotypes==_ for _ in GroupsOrder)):
        I = V*2+G
        if V:
            if KeyPeak == 'N40':
                AxesQnt[I].fill_between(
                    (YPos[0][0],YPos[-1][-1]),
                    (20,20),(50,50),
                    color='k', lw=0, alpha=0.06
                )
                AxesQnt[I].text(
                    np.mean(AxesQnt[I].get_xlim()),
                    55,
                    f'{KeyPeak} component',
                    {'color':'k', 'alpha':0.3, 'ha':'center', 'va':'center'}
                )

        for Tr,Treatment in enumerate(TreatmentsOrder):
            Y = []
            for P, Peak in enumerate(PeakLabels):
                if not len(Y):
                    Y = Var[P, ((Treatments == Treatment)*Group)]
                    Y = Y.reshape((Y.shape[0], 1))
                else:
                    Y = np.vstack((Y.T, Var[P, ((Treatments == Treatment)*Group)])).T

            if 'scatter' in [_.lower() for _ in PlotType]:
                AxesQnt[I] = Plot.ScatterMean(
                    Y.T, YPos[Tr],
                    ColorsMarkers=ColorsCc, Markers=['.']*2,
                    Paired=True, Ax=AxesQnt[I], **SMArgs
                )

            AR = Anova['PWCs']['Click']
            AR = AR[Stats.spk(AR)]
            pi = (AR['Genotype']==GroupsOrder[G])*(AR['Treatment']==Treatment)
            if np.where(pi)[0].shape[0]>1:
                raise ValueError('There should be only one genotype/treatment combination.')
            p = AR['p.adj'][np.where(pi)[0][0]]

            if p < 0.05:
                y = max(AxesQnt[I].get_ylim())
                p = '{:.1e}'.format(p)
                Plot.SignificanceBar(
                    YPos[Tr], [y*SigSpace[V][G]]*2, p,
                    AxesQnt[I], TextArgs=SigA, TicksDir=None,
                    LineTextSpacing=1.06
                )

            AxesQnt[I].set_xticks(XTicks)


for A,Ax in enumerate(AxesQnt):
    AxArgs = {}

    if A<2: AxArgs['ylabel'] =  f'{KeyPeak} Amplitude [µV]'
    else: AxArgs['ylabel'] =  f'{KeyPeak} Latency [ms]'

    if KeyPeak != 'P80':
        if A<2: AxArgs['ylim'] =  [0,600]
        else: AxArgs['ylim'] =  [0,60]

    if KeyPeak == 'P80':
        if A<2: AxArgs['ylim'] =  [0,350]
        else:
            AxArgs['ylim'] =  [20,120]
            AxArgs['yticks'] =  np.linspace(20,120,6)

    Plot.Set(Ax=Ax, AxArgs=AxArgs)


LegLoc = 'lower right' if KeyPeak == 'N40' else 'upper left'
LegBbox = (0, 0.0, 1.0, 0.95) if KeyPeak == 'N40' else (0, 0.1, 1.0, 0.9)
Legend = [
    Plot.GenLegendHandle(
        color=Colors['1stClick'], alpha=0.5, label='Click 1',
        marker='.', linestyle=''
    ),
    Plot.GenLegendHandle(
        color=Colors['2ndClick'], alpha=0.5, label='Click 2',
        marker='.', linestyle=''
    )
]
AxesQnt[2].legend(handles=Legend, handlelength=1.0, labelcolor=ColorsCc, **LA)

for Ax in AxesQnt.ravel():
    Ax.set_xticklabels([
        _.replace('Cann+Nic','C+N') for _ in TreatmentsNames
    ])

FA = {'fontsize': 10, 'weight': 'bold'}
for A in range(2):
    AxesQnt[A].set_title(
        GroupsNamesTex[A], {**FA, **{'color':ColorsC[A],'usetex':True}}
    )



# Ratios
XPos = [
    np.arange(
        0,
        len(TreatmentsOrder)*(len(GroupsOrder)+1),
        len(GroupsOrder)+1
    )+_
    for _ in range(2)
]
YPos = [[i*3,i*3+1] for i in range(len(TreatmentsOrder))]
XTicks = [np.mean(_) for _ in YPos]
SigSpace = [[1.15, 1.05], [1.05, 1.05]]

Anova = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/ERPs-RatioAmp-Genotype_Treatment-{KeyPeak}-Trial{TrialToRun}.dict')
Anova = IO.Txt.DictListsToArrays(Anova)

Thr = 3
z = Stats.sst.zscore(PeakAmpRatios)
z[(z<-Thr)+(z>Thr)] = np.nan
z[~np.isnan(z)] = 1
PeakAmpRatios = PeakAmpRatios*z

for G,Group in enumerate(GroupsOrder):
    ThisGroup = Genotypes == Group
    K = np.where((KeyPeaks==KeyPeak))[0][0]
    Y = np.array([PeakAmpRatios[(Treatments==_)*ThisGroup,K] for _ in TreatmentsOrder])
    Plot.ScatterMean(
        Y, XPos[G], ColorsMarkers=[ColorsC[G]]*4, Markers=['o']*4,
        Ax=AxesRatios[0], **SMArgs
    )

XLim = [XPos[0][0]-0.5, XPos[-1][-1]+0.5]
AxesRatios[0] = AnovaStars(Anova, AxesRatios[0], None, 1.06)
AxesRatios[0].plot(XLim, [0,0], color='silver', linestyle='--')

AxesRatios[0].set_ylabel(RatiosLabels[0])
AxesRatios[0].set_xlim([XPos[0][0]-0.5, XPos[-1][-1]+0.5])
AxesRatios[0].set_xticks(np.mean(XPos, axis=0))

if KeyPeak=='N40': AxesRatios[0].set_ylim((-10,100))

AxArgs = {
    'xticklabels': TreatmentsNames,
}

Plot.Set(Ax=AxesRatios[0], AxArgs=AxArgs)
AxesRatios[0].set_title(f'{KeyPeak} amplitude ratio', weight='bold')

Legend = [
    Plot.GenLegendHandle(color=ColorsC[0], alpha=0.4, label=GroupsNames[0], marker='o', linestyle=''),
    Plot.GenLegendHandle(color=ColorsC[1], alpha=0.4, label=GroupsNames[1], marker='o', linestyle='')
]
AxesRatios[0].legend(handles=Legend, **LA)


# Amplitudes
SigSpace = [1.05, 1.05]
GNs = [
    gn.replace(GroupsBreak[g],GroupsBreak[g]+'\n')
    if len(GroupsBreak[g]) else gn
    for g,gn in enumerate(GroupsNames)
]

z = Stats.sst.zscore(PeakAmps.ravel())
z[(z<-3)+(z>3)] = np.nan
z[~np.isnan(z)] = 1
z = z.reshape(PeakAmps.shape)
z = np.hstack([[np.prod(z,axis=0)]*2])
PeakAmps = PeakAmps*z

py = []
for G,Group in enumerate(GroupsOrder):
    Y = [
        PeakAmps[c,(Treatments==t)*(Genotypes==Group),KeyInd]
        for t in TreatmentsOrder for c in range(2)
    ]
    AxesRatios[1] = Plot.MeanSEM(
        Y, [],
        {'marker':'','color':ColorsC[G],'markerfacecolor':'none','lw':1},
        None,
        Ax=AxesRatios[1]
    )
    for yi,y in enumerate([np.nanmean(_) for _ in Y]):
        AxesRatios[1].plot(
            yi, y, marker=PeakMarkers[yi%2],
            markeredgecolor=Colors[ClicksOrder[yi%2]],  markerfacecolor=Colors[ClicksOrder[yi%2]], linestyle=None
        )

    py.append(np.nanmean([_ for y in Y for _ in y]))


AR = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/ERPs-Amp-Click_Genotype_Treatment-{KeyPeak}-Trial{TrialToRun}.dict')
AR = IO.Txt.DictListsToArrays(AR)
g = AR[Stats.sak(AR)]['Effect']=='Genotype'
p = AR[Stats.sak(AR)]['p'][g][0]
if p<0.05:
    p = '{:.1e}'.format(p)
    py = np.array(py)*[0.8, 1.2]
    px = [len(TreatmentsOrder)*2-0.5]*2
    Plot.SignificanceBar(
        px, py, p,
        AxesRatios[1],
        {**SigA, **{'va':'center', 'rotation':270}},
        TicksDir='left',
        LineTextSpacing=1.02,
    )


AxesRatios[1].set_xticks(np.arange(len(TreatmentsOrder))*2+0.5)
AxesRatios[1].set_xticklabels(TreatmentsNames)
YLim = (50,300) if KeyPeak == 'N40' else (0,200)
AxesRatios[1].set_ylim(YLim)
AxesRatios[1].set_ylabel('Voltage [µV]')
AxesRatios[1].set_title(f'{KeyPeak} amplitude', weight='bold')
Plot.Set(Ax=AxesRatios[1])

Legend = [
    Plot.GenLegendHandle(color=ColorsC[0], label=GroupsNames[0], marker='', linestyle='-'),
    Plot.GenLegendHandle(color=ColorsC[1], label=GroupsNames[1], marker='', linestyle='-'),
    Plot.GenLegendHandle(color=Colors[ClicksOrder[0]], label=PeakLabels[0], marker=PeakMarkers[0], linestyle=''),
    Plot.GenLegendHandle(color=Colors[ClicksOrder[1]], label=PeakLabels[1], marker=PeakMarkers[1], linestyle='')
]
AxesRatios[1].legend(loc='upper right', ncol=2, handles=Legend, handlelength=1.0, **LA)



# Set axes
Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add text
Pos = np.array([_.get_position().corners() for _ in AxesTraces.ravel()]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
FA = {'size': 8, 'va':'center', 'ha': 'center', 'weight': 'bold', 'rotation': 90, 'usetex':True}
Fig.text(
    Pos[0][0]-0.03, Pos[0][1]-0.03, GroupsNamesTex[0],
    fontdict=FA, color=ColorsC[0]
)
Fig.text(
    Pos[0][0]-0.03, Pos[4][1]-0.04, GroupsNamesTex[1],
    fontdict=FA, color=ColorsC[1]
)


# Add labels
AxAll = np.concatenate(([AxesTraces[0,0]], AxesQnt.ravel(), AxesRatios.ravel()))
Pos = np.array([_.get_position().corners() for _ in AxAll]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.004, Pos[2][0]-0.08, Pos[6][0]-0.08]
Y = [0.96, Pos[1][1]+0.01, Pos[3][1]+0.00, Pos[5][1]+0.00]
AxesQntPos = [
    [X[0], Y[0]],
    [X[0], Y[1]], [X[1], Y[1]],
    [X[0], Y[2]], [X[1], Y[2]],
    [X[0], Y[3]], [X[2], Y[3]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesQntPos)]
Plot.SubLabels(Fig, AxesQntPos, Letters, FontArgs=SPLA)


# Write fig
if Save: Fig.savefig(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% [ERPs] Figure Ratio Lat ====================================================
KeyPeaks = ['N40', 'P80']
FigName = f'CiralliEtAl_FigRatioLat'

KeyData = 'DataChannels'
PeakLabels = ['1st click', '2nd click']
PeakMarkers = ['s', '^']
PlotType = ['scatter']
SMArgs = dict(Alpha=0.4, ErrorArgs=dict(capsize=3))


# Load data
Animals, Genotypes, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/GroupInfo/{_}.dat')[0]
    for _ in ('Animals', 'Genotypes', 'Treatments', 'Trials')
)

KeyPeaksOrder, PeakLats, PeakLatRatios = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeaks/{_}.dat')[0]
    for _ in ('PeaksOrder', 'PeakLats', 'PeakLatRatios')
)


if type(TrialToRun) == int and TrialToRun > 0:
    AnimalsU = [
        Animal for Animal in np.unique(Animals)
        if max(Trials[Animals==Animal]) < TrialToRun
    ]
    for Animal in AnimalsU:
        TrialMax = max(Trials[Animals==Animal])
        Trials[(Trials==TrialMax)*(Animals==Animal)] = TrialToRun


if type(TrialToRun) == int:
    t = Trials == TrialToRun

    Animals, Genotypes, Treatments = (_[t] for _ in (Animals, Genotypes, Treatments))
    PeakLats = PeakLats[:,t,:]
    PeakLatRatios = PeakLatRatios[t,:]


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.5]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(len(KeyPeaks),2)

AxesQnt = np.array([
    Fig.add_subplot(GS[L,C])
    for L in range(len(KeyPeaks)) for C in range(2)
])


XPos = [
    np.arange(
        0,
        len(TreatmentsOrder)*(len(GroupsOrder)+1),
        len(GroupsOrder)+1
    )+_
    for _ in range(2)
]

XLim = [XPos[0][0]-0.5, XPos[-1][-1]+0.5]
SigSpace = [1.05, 1.05]

GNs = [
    gn.replace(GroupsBreak[g],GroupsBreak[g]+'\n')
    if len(GroupsBreak[g]) else gn
    for g,gn in enumerate(GroupsNames)
]


for KP,KeyPeak in enumerate(KeyPeaks):
    ColorsC = [
        Colors['WT'].copy(),
        Colors['KO'].copy()
    ]

    if KeyPeak == 'N40':
        ColorsC[1] *= 2
    elif KeyPeak == 'P80':
        ColorsC[1] = [150/255, 0, 170/255]

    # Ratios
    Anova = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/ERPs-RatioLat-Genotype_Treatment-{KeyPeak}-Trial{TrialToRun}.dict')
    Anova = IO.Txt.DictListsToArrays(Anova)

    Thr = 3
    z = Stats.sst.zscore(PeakLatRatios)
    z[(z<-Thr)+(z>Thr)] = np.nan
    z[~np.isnan(z)] = 1
    Ax = PeakLatRatios*z

    for G,Group in enumerate(GroupsOrder):
        ThisGroup = Genotypes == Group
        K = np.where((KeyPeaksOrder==KeyPeak))[0][0]
        Y = np.array([Ax[(Treatments==_)*ThisGroup,K] for _ in TreatmentsOrder])
        Y = Y*-1 # represent delay instead of advance
        Plot.ScatterMean(
            Y, XPos[G], ColorsMarkers=[ColorsC[G]]*4, Markers=['o']*4,
            Ax=AxesQnt[KP*2], **SMArgs
        )

    AxesQnt[KP*2] = AnovaStars(Anova, AxesQnt[KP*2], None, 1.06)
    AxesQnt[KP*2].plot(XLim, [0,0], color='silver', linestyle='--')

    AxesQnt[KP*2].set_ylabel('2nd peak delay [%]')
    AxesQnt[KP*2].set_xlim(XLim)
    AxesQnt[KP*2].set_xticks(np.mean(XPos, axis=0))

    AxArgs = {
        'xticklabels': TreatmentsNames,
    }

    Plot.Set(Ax=AxesQnt[KP*2], AxArgs=AxArgs)
    AxesQnt[KP*2].set_title(f'{KeyPeak} Latency ratio', weight='bold')

    Legend = [
        Plot.GenLegendHandle(color=ColorsC[0], alpha=0.4, label=GroupsNames[0], marker='o', linestyle=''),
        Plot.GenLegendHandle(color=ColorsC[1], alpha=0.4, label=GroupsNames[1], marker='o', linestyle='')
    ]
    AxesQnt[KP*2].legend(loc='lower right', handles=Legend, **LA)

    # Latencies
    KeyInd = KeyPeaksOrder.tolist().index(KeyPeak)
    py = []
    for G,Group in enumerate(GroupsOrder):
        Y = [
            PeakLats[c,(Treatments==t)*(Genotypes==Group),KeyInd]
            for t in TreatmentsOrder for c in range(2)
        ]
        AxesQnt[KP*2+1] = Plot.MeanSEM(
            Y, [],
            {'marker':'','color':ColorsC[G],'markerfacecolor':'none','lw':1},
            None,
            Ax=AxesQnt[KP*2+1]
        )
        for yi,y in enumerate([np.nanmean(_) for _ in Y]):
            AxesQnt[KP*2+1].plot(
                yi, y, marker=PeakMarkers[yi%2],
                markeredgecolor=Colors[ClicksOrder[yi%2]], markerfacecolor=Colors[ClicksOrder[yi%2]], linestyle=None
            )

        py.append(np.nanmean([_ for y in Y for _ in y]))


    AR = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/ERPs-Lat-Click_Genotype_Treatment-{KeyPeak}-Trial{TrialToRun}.dict')
    AR = IO.Txt.DictListsToArrays(AR)
    g = AR[Stats.sak(AR)]['Effect']=='Genotype'
    p = AR[Stats.sak(AR)]['p'][g][0]
    if p<0.05:
        py = np.array(py)*[0.8, 1.2]
        px = [len(TreatmentsOrder)*2-0.5]*2
        Plot.SignificanceBar(
            px, py, p,
            AxesQnt[KP*2+1],
            {**SigA, **{'va':'center', 'rotation':270}},
            TicksDir='left',
            LineTextSpacing=1.02,
        )


    AxesQnt[KP*2+1].set_xticks(np.arange(len(TreatmentsOrder))*2+0.5)
    AxesQnt[KP*2+1].set_xticklabels(TreatmentsNames)
    AxesQnt[KP*2+1].set_ylabel('Latency [ms]')
    AxesQnt[KP*2+1].set_title(f'{KeyPeak} Latency', weight='bold')

    Legend = [
        Plot.GenLegendHandle(color=ColorsC[0], label=GroupsNames[0], marker='', linestyle='-'),
        Plot.GenLegendHandle(color=ColorsC[1], label=GroupsNames[1], marker='', linestyle='-'),
        Plot.GenLegendHandle(color=Colors[ClicksOrder[0]], label=PeakLabels[0], marker=PeakMarkers[0], linestyle=''),
        Plot.GenLegendHandle(color=Colors[ClicksOrder[1]], label=PeakLabels[1], marker=PeakMarkers[1], linestyle='')
    ]
    AxesQnt[KP*2+1].legend(loc='upper right', ncol=2, handles=Legend, handlelength=1.0, **LA)


AxesQnt[0].set_ylim((-70,10))
AxesQnt[1].set_ylim((10,60))
AxesQnt[2].set_ylim((-70,10))
AxesQnt[3].set_ylim((20,105))
for Ax in AxesQnt: Plot.Set(Ax=Ax)

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add labels
Pos = np.array([_.get_position().corners() for _ in AxesQnt.ravel()]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.004, Pos[1][0]-0.08]
Y = [0.96, Pos[2][1]+0.01]
AxesQntPos = [
    [X[0], Y[0]], [X[1],Y[0]],
    [X[0], Y[1]], [X[1], Y[1]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesQntPos)]
Plot.SubLabels(Fig, AxesQntPos, Letters, FontArgs=SPLA)


# Write fig
if Save: Fig.savefig(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% [ERPs] Figure Amp Treat ====================================================
KeyPeaks = ['N40', 'P80']
FigName = f'CiralliEtAl_FigAmpTreat'

ClickLabels = ['Click 1', 'Click 2']
PlotType = ['scatter']
ColorsC = [Colors['WT'], Colors['KO']]
ColorsCc = [Colors['1stClick'], Colors['2ndClick']]
FigSize = [A4Size[0], A4Size[1]*0.5]

SigSpace = np.array([
    [1.05, 1.02, 1.05, 1.05],
    [1.15, 1.05, 1.05, 1.05],
])
SigSpace += 0.02

PeakLabels = ['1stClick', '2ndClick']
RatiosLabels = ['2nd peak suppr. [%]', '2nd peak delay [%]']

# Load data
Animals, Genotypes, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/GroupInfo/{_}.dat')[0]
    for _ in ('Animals', 'Genotypes', 'Treatments', 'Trials')
)

KeyPeaksOrder, PeakAmps, PeakLats = (
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeaks/{_}.dat')[0]
    for _ in ('PeaksOrder', 'PeakAmps', 'PeakLats')
)

if type(TrialToRun) == int and TrialToRun > 0:
    AnimalsU = [
        Animal for Animal in np.unique(Animals)
        if max(Trials[Animals==Animal]) < TrialToRun
    ]
    for Animal in AnimalsU:
        TrialMax = max(Trials[Animals==Animal])
        Trials[(Trials==TrialMax)*(Animals==Animal)] = TrialToRun


KeyList = KeyPeaksOrder.tolist()
PeakAmps = abs(PeakAmps)


if type(TrialToRun) == int:
    t = Trials == TrialToRun
    Animals, Genotypes, Treatments = (_[t] for _ in (Animals, Genotypes, Treatments))
    PeakAmps, PeakLats = (_[:,t,:] for _ in (PeakAmps, PeakLats))


Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(3, len(KeyPeaks)*2+1, height_ratios=(0.8,5,5), width_ratios=(1.5,)+(5,)*len(KeyPeaks)*2)
Axes = np.array([
    [Fig.add_subplot(GS[L+1,C+1]) for C in range(4)]
    for L in range(2)
])
Axes[0,1], Axes[1,0] = Axes[1,0], Axes[0,1]
Axes[0,3], Axes[1,2] = Axes[1,2], Axes[0,3]

for KP, KeyPeak in enumerate(KeyPeaks):
    KeyInd = KeyList.index(KeyPeak)

    Anova = IO.Txt.Read(f'{AnalysisPath}/GroupData/Stats/ERPs-Amp-Click_Genotype_Treatment-{KeyPeak}-Trial{TrialToRun}.dict')
    Anova = IO.Txt.DictListsToArrays(Anova)
    TreatPWC = Anova['PWCs']['Treatment']
    TreatPWC = TreatPWC[Stats.spk(TreatPWC)]

    for G,Group in enumerate((Genotypes==_ for _ in GroupsOrder)):
        AxC = G+(len(GroupsOrder)*KP)

        Y = []
        for Tr,Treatment in enumerate(TreatmentsOrder):
            y = PeakAmps[:,:,KeyInd][:, ((Treatments == Treatment)*Group)]
            if not len(Y):
                Y = np.zeros(y.shape+(len(TreatmentsOrder),), y.dtype)

            Y[:,:,Tr] = y

        for P, PeakLabel in enumerate(PeakLabels):
            AxL = P

            z = Stats.sst.zscore(Y[P,:,:].ravel())
            z[(z<-3)+(z>3)] = np.nan
            z[~np.isnan(z)] = 1
            z = z.reshape(Y[P,:,:].shape)
            z = np.hstack([[np.prod(z,axis=1)]*Y.shape[2]]).T
            Y[P,:,:] = Y[P,:,:]*z

            if 'scatter' in [_.lower() for _ in PlotType]:
                Axes[AxL][AxC] = Plot.ScatterMean(
                    Y[P,:,:].T, range(len(TreatmentsOrder)),
                    ColorsMarkers=[ColorsCc[P]]*len(TreatmentsOrder),
                    Markers=['.']*len(TreatmentsOrder), Paired=True,
                    Ax=Axes[AxL][AxC]
                )

            Pairs = list(Stats.combinations(range(len(TreatmentsOrder)), 2))
            for Pa,Pair in enumerate(Pairs):
                apt = (
                    (TreatPWC['Click']==str(P+1)) *
                    (TreatPWC['Genotype']==GroupsOrder[G])
                )
                apt = apt*(
                    (TreatPWC['group1']==TreatmentsOrder[Pair[0]]) +
                    (TreatPWC['group2']==TreatmentsOrder[Pair[0]])
                )
                apt = apt*(
                    (TreatPWC['group1']==TreatmentsOrder[Pair[1]]) +
                    (TreatPWC['group2']==TreatmentsOrder[Pair[1]])
                )
                p = TreatPWC['p.adj'][apt]
                if len(p) > 1:
                    raise ValueError('There should be only one p-value here.')
                p = p[0]

                if p < 0.05:
                    y = max(Axes[AxL][AxC].get_ylim())
                    ps = '{:.1e}'.format(p)
                    Plot.SignificanceBar(
                        Pair, [y*SigSpace[AxL][AxC]]*2, ps,
                        Axes[AxL][AxC], TextArgs=SigA,
                        TicksDir=None, LineTextSpacing=1.05
                    )

Axes[0,1], Axes[1,0] = Axes[1,0], Axes[0,1]
Axes[0,3], Axes[1,2] = Axes[1,2], Axes[0,3]

for L,Line in enumerate(Axes):
    for A,Ax in enumerate(Line):
        AxArgs = {
            'xticks': range(len(TreatmentsOrder))
        }

        if A < 2: AxArgs['ylim'] =  [0,515]
        elif A > 1:
            AxArgs['ylim'] =  [0,355]
            AxArgs['yticks'] =  np.linspace(0,350,8)

        if A == 0: AxArgs['ylabel'] = 'Voltage [µV]'

        Plot.Set(Ax=Ax, AxArgs=AxArgs)

for A,Ax in enumerate(Axes.ravel()):
    Ax.set_title(ClickLabels[A%2], pad=1, color=Colors[ClicksOrder[A%2]])

for Ax in Axes.ravel():
    Ax.set_xticklabels(TreatmentsNames, rotation=-15)

for Ax in Axes[:,[1,3]].ravel():
    Ax.spines['left'].set_visible(False)
    Ax.set_yticks(())


Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add boxes around subplot titles
Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [
    [
        Pos[0,1,_], Pos[1,1,_]*1.0175, Pos[0,2,_]-Pos[0,1,_],
        (Pos[1,1,_]-Pos[1,0,_])*0.125
    ]
    for _ in range(Pos.shape[2])
]

pax = (0,2)
for _ in pax:
    Pos[_][1] *= 1.02
    Pos[_][2] = (Pos[_+1][0]+Pos[_+1][2])-Pos[_][0]
    Pos[_][3] *= 0.9

AxRects = [Fig.add_axes(Pos[_], zorder=-1) for _ in pax]
for A,Ax in enumerate(AxRects):
    Ax.add_patch(plt.Rectangle((0,0),1,1, facecolor='w', edgecolor='k', clip_on=False, linewidth=1))
    Ax.text(0.5, 0.4, f'{KeyPeaks[A]} Amplitude', ha='center', va='center')
    Ax.axis('off')

Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [[0.005, Pos[1,0,_], 0.025, Pos[1,1,_]-Pos[1,0,_]] for _ in range(Pos.shape[2])]

pax = (0,4)
for _ in pax: Pos[_][1]*=1
AxRects = [Fig.add_axes(Pos[_], zorder=-1) for _ in pax]
for A,Ax in enumerate(AxRects):
    Ax.add_patch(plt.Rectangle((0,0),1,1, facecolor='w', edgecolor=Colors[GroupsOrder[A%2]], clip_on=False, linewidth=1))
    Ax.text(0.6, 0.5, GroupsNames[A%2], color=Colors[GroupsOrder[A%2]], ha='center', va='center', rotation=90)
    Ax.axis('off')


# Add sublabels
Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.005, Pos[2][0]-0.06]
Y = [0.97]
AxesPos = [
    [X[0], Y[0]], [X[1], Y[0]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write
if Save:
    for E in Ext: Fig.savefig(f'{FiguresPath}/{FigName}.{E}')
plt.show()



#%% EOF ========================================================================
