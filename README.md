# SensoryGatingOnSLC10A4

Malfatti T, Ciralli B (2023). SensoryGatingOnSLC10A4 (v1.0.0). Zenodo. (Coming soon)

This repository contains all code used for the experiments and analysis of the publication

Slc10a4 knockout mice show abnormal auditory gating of N40 and P80 event-related potentials
(under review)

