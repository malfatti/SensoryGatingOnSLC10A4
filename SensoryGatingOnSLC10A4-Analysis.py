#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@date: 20190502
@license: GNU GPLv3 <https://gitlab.com/malfatti/SensoryGatingOnSLC10A4/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/SensoryGatingOnSLC10A4

Slc10a4 knockout mice show abnormal auditory gating of N40 and P80 event-related potentials
"""



#%% Prepare ====================================================================
ScriptName = 'SensoryGatingOnSLC10a4'

print(f'[{ScriptName}] Loading dependencies...')
import numpy as np, os
from glob import glob
from sciscripts.Analysis import Analysis, ERPs, Stats
from sciscripts.IO import IO
from xml.etree import ElementTree

from sciscripts.Analysis.Plot import Plot
plt = Plot.Return('plt')


# General parameters
DataPath = os.environ['DATAPATH']+'/SensoryGatingOnSLC10a4'
AnalysisPath = os.environ['ANALYSISPATH']+'/SensoryGatingOnSLC10a4'
AnalysisGroup = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Analysis/SensoryGatingOnSLC10a4'
GenotypesOrder = ('WT', 'KO')
TreatmentsOrder = ('NaCl', 'Ket5mg', 'Ket20mg')
PeaksOrder = ('P20', 'N40', 'P80')

# Plot parameters
Ext = ('svg','pdf')

# Experiment parameters
ExpInfo = {
    'Animals': np.array([
        '36237', '36273', '36280', '40055', '40056',
        '40061', '40065', '40067', '40153', '40162',
        '36018', '36537', '36458', '36257'
    ]),

    'Genotypes': np.array([
        'KO', 'WT', 'KO', 'KO', 'KO', 'WT', 'WT',
        'WT', 'KO', 'WT', 'WT', 'KO', 'KO', 'KO'
    ]),

    'Sex': np.array([
        'F', 'F', 'F', 'M', 'M', 'M', 'M',
        'F', 'F', 'F', 'F', '', 'F', 'M'
    ]),

    'CompleteSet': np.array([
        True,  True,  True,  True,  True,
        False,  True,  True, True, True,
        True, True, True, True
    ]),

    'EPhys': {
        'BaselineDur': 0.2,
        'PeakWindows': ((0,0.12), (0.5,0.62)),
        'P20Window': np.array([0.0, 0.03]),
        'N40Window': np.array([0.01, 0.06]),
        'P80Window': np.array([0.03, 0.1]),

        'Probes': np.array([
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-100-413-CM16',
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-50-703-CM16',
            'A1x16-3mm-50-413-CM16',
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-100-177-CM16LP',
            'A1x16-3mm-100-413-CM16',
            'A4x4-3mm-50-703-CM16',
            'A1x16-3mm-100-177-CM16LP',
            'A4x4-3mm-50-703-CM16',

        ]),

        'BrokenChs': np.array([
            [3,4,5,9,14,15],
            [6,7,8,9,10,11,12,13,14,15,16],
            [5,6,7,8,13],
            [],
            [1,9,10],
            [11], [5,6], [],
            [3,4,5,7,8,9,10,11,12,13,14,15,16],
            [1,5,13],
            [8,10,12,13,14,15],
            [1,2,3,4,9,10,11,12,13,14,15,16],
            [], []
        ], dtype=object)
    },

    'Video': {
        'ArenaSize_cm': [27, 16],
        'ArenaSize_px': [535, 317],
        'IMGSize': [800, 600],
        'FPS': 15,
        'CornerThreshold': 5,
    }
}

ExpInfo['Video']['PixelSize'] = np.divide(
    ExpInfo['Video']['ArenaSize_cm'],
    ExpInfo['Video']['ArenaSize_px']
).mean()


def GetDataFromXMLs(File):
    """
    Function for getting tracking results from Mice Profiler Tracker (ICY plugin)
    """
    Data = {}
    CoordTree = ElementTree.parse(File)
    CoordRoot = CoordTree.getroot()

    Data['Coords'] = {}
    Animal = CoordRoot[2][0]
    if Animal.tag not in Data['Coords']: Data['Coords'][Animal.tag] = {}

    Keys = list(Animal[0].keys())
    Data['Coords'][Animal.tag] = {Key: np.array([F.get(Key) for F in Animal], dtype='float32')
                                  for Key in Keys if Key != 't'}

    Data['t'] = np.array([F.get('t') for F in Animal], dtype='float32')

    return(Data)


print(f'[{ScriptName}] Done.')



#%% Data info ==================================================================

# ERPs
ERPFolders = sorted(glob(DataPath+'/ERPsMov/2*ERPs/20??-*'))
ERPFolders = np.array([
    _ for _ in ERPFolders
    if _.split('_')[-1] in TreatmentsOrder
])

Animals = np.array([
    _.split('/')[-2].split('-')[1] for _ in ERPFolders
])

CompleteSet = np.array([
    _ in ExpInfo['Animals'][ExpInfo['CompleteSet']]
    for _ in Animals
])

ERPFolders, Animals = (_[CompleteSet] for _ in (ERPFolders, Animals))

Treatments = np.array([_.split('_')[-1] for _ in ERPFolders])

Genotypes = np.array([
    ExpInfo['Genotypes'][ExpInfo['Animals']==_][0]
    for _ in Animals
])

Trials = np.vstack((Animals, Genotypes, Treatments)).T.tolist()
Trials = np.array([
    len([e for e,E in enumerate(Trials[:a]) if E == Trials[a]])
    for a in range(len(Trials))]
)


# Trackings
XMLFiles = sorted(glob(AnalysisPath+'/ERPsMov/**/*.xml', recursive=True))
XMLFiles = np.array([
    _ for _ in XMLFiles
    if 'xml tracking backup' not in _
    and '_non' in _
])

XMLAnimals = np.array([
    _.split('/')[-4].split('-')[1] for _ in XMLFiles
])

XMLTreatments = np.array([_.split('/')[-3].split('_')[-1] for _ in XMLFiles])

XMLGenotypes = np.array([
    ExpInfo['Genotypes'][ExpInfo['Animals']==_][0]
    for _ in XMLAnimals
])

XMLTrials = np.vstack((XMLAnimals, XMLGenotypes, XMLTreatments)).T.tolist()
XMLTrials = np.array([
    len([e for e,E in enumerate(XMLTrials[:a]) if E == XMLTrials[a]])
    for a in range(len(XMLTrials))]
)

XMLToConcat = [
    [xf]+[
        _ for _ in XMLFiles
        if _ != xf
        and os.path.commonpath([xf,_]) == '/'.join(xf.split('/')[:-1])
    ]
    for xf in XMLFiles
]

XMLToConcat = np.unique([sorted(_) for _ in XMLToConcat]).tolist()

XMLSets = np.array([
    np.where([xf in xc for xc in XMLToConcat])[0][0]
    for xf in XMLFiles
])

for xf,XF in enumerate(XMLToConcat):
    i = [XMLFiles.tolist().index(_) for _ in XF]
    XMLTrials[i] = XMLTrials[i].min()


# Save
Info = {K: globals()[K] for K in (
    'Animals', 'Treatments', 'Genotypes', 'Trials',
    'XMLAnimals', 'XMLTreatments', 'XMLGenotypes', 'XMLTrials', 'XMLSets',
)}
Info['Folders'] = ERPFolders
Info['XMLFiles'] = XMLFiles
IO.Bin.Write(Info, f'{AnalysisGroup}/GroupData/GroupInfo')
IO.Txt.Write(ExpInfo, f'{AnalysisGroup}/GroupData/GroupInfo/ExpInfo.dict')



#%% [Locomotion] Get Distance, Area and Speed ==================================
XMLAnimals, XMLGenotypes, XMLTreatments = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{K}.dat')[0]
    for K in ('XMLAnimals', 'XMLGenotypes', 'XMLTreatments')
)

XMLFiles, XMLTrials, XMLSets = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{K}.dat')[0]
    for K in ('XMLFiles', 'XMLTrials', 'XMLSets')
)

XMLData = {
    _: [] for _ in (
        'X', 'Y', 't', 'Dist', 'Area',
        'Animals', 'Genotypes', 'Treatments', 'Trials'
    )
}

print('Processing XMLs...')
SetDone = []
for F,File in enumerate(XMLFiles):
    print(f'    Folder {F+1} of {len(XMLFiles)}...')
    if XMLSets[F] in SetDone: continue

    i = XMLSets == XMLSets[F]
    X = np.array([], dtype='float32')
    Y = np.array([], dtype='float32')
    t = np.array([], dtype='float32')

    for XMLF in XMLFiles[i]:
        Data = GetDataFromXMLs(XMLF)

        Xf = Data['Coords']['MOUSEA']['bodyx'] * ExpInfo['Video']['PixelSize']
        Yf = Data['Coords']['MOUSEA']['bodyy'] * ExpInfo['Video']['PixelSize']
        X = np.concatenate((X, Xf))
        Y = np.concatenate((Y, Yf))

        Offset = t[-1] + 1/ExpInfo['Video']['FPS'] if len(t) else 0
        tf = (Data['t']/ExpInfo['Video']['FPS']) + Offset
        t = np.concatenate((t, tf))

    if not len(t): continue
    Dist = np.concatenate(([0],Analysis.EucDist(X, Y)[0]))
    Area = Analysis.PolygonArea(X, Y)

    XMLData['X'].append(X)
    XMLData['Y'].append(Y)
    XMLData['t'].append(t)
    XMLData['Dist'].append(Dist)
    XMLData['Area'].append(Area)
    XMLData['Animals'].append(XMLAnimals[F])
    XMLData['Genotypes'].append(XMLGenotypes[F])
    XMLData['Treatments'].append(XMLTreatments[F])
    XMLData['Trials'].append(XMLTrials[F])

    SetDone.append(XMLSets[F])


XMLData = {
    K: np.array(V) if K not in ('X', 'Y', 't', 'Dist') else V
    for K,V in XMLData.items()
}

XMLData['Speed'] = [D/XMLData['t'][d]  for d,D in enumerate(XMLData['Dist'])]
for s,S in enumerate(XMLData['Speed']): XMLData['Speed'][s][np.isnan(S)] = 0
XMLData['DistMean'] = np.array([_.mean() for _ in XMLData['Dist']])
XMLData['DistSum'] = np.array([_.sum() for _ in XMLData['Dist']])
XMLData['SpeedMean'] = XMLData['DistSum'] / np.array([_[-1] for _ in XMLData['t']])
IO.Bin.Write(XMLData, f'{AnalysisGroup}/GroupData/Trackings')



#%% [Locomotion] Stats =========================================================
FacOrder = ('Genotype', 'Treatment')
FacPaired = (False, True)

AOut = f'{AnalysisGroup}/GroupData/Stats'

Animals, Treatments, Genotypes, Trials = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/Trackings/{_}.dat')[0]
    for _ in ('Animals', 'Treatments', 'Genotypes', 'Trials')
)

DistSum, DistMean, SpeedMean, Area = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/Trackings/{_}.dat')[0]
    for _ in ('DistSum', 'DistMean', 'SpeedMean', 'Area')
)

for Trial in np.unique(Trials):
    if Trial > 0:
        AnimalsU = [
            Animal for Animal in np.unique(Animals)
            if max(Trials[Animals==Animal]) < Trial
        ]
        for Animal in AnimalsU:
            TrialMax = max(Trials[Animals==Animal])
            Trials[(Trials==TrialMax)*(Animals==Animal)] = Trial


    print(f'[{ScriptName}] Processing Dists and Speeds...')
    aftr = Trials == Trial

    FXs = {
        'Genotype': Genotypes,
        'Treatment': Treatments,
    }
    FXs = {K: V[aftr] for K,V in FXs.items()}
    afs = [FXs[_] for _ in FacOrder]
    aid = Animals[aftr]

    for D,Data in (
            ('DistSum', DistSum), ('DistMean', DistMean),
            ('SpeedMean',SpeedMean),('Area',Area)
        ):
        print(f'[{ScriptName}]     Trial {Trial} {D}...')
        ad = Data[aftr]

        try:
            A = Stats.AnOVa(ad, afs, aid, FacPaired, 'auto', FacOrder)
            for _ in Stats.GetAnovaReport(A,FacOrder): print(_)

            IO.Txt.Write(A, f"{AOut}/Behavior-{D}-{'_'.join(FacOrder)}-Trial{Trial}.dict")
            IO.Bin.Write(
                dict(
                    Data=ad,
                    FacNames=np.array(FacOrder),
                    FacPaired=np.array(FacPaired),
                    Ids=aid,
                    FXs=np.array(afs).T
                ),
                f"{AOut}/Behavior-{D}-{'_'.join(FacOrder)}-Trial{Trial}_Data"
            )

        except Exception as e:
            print(e)
            print(f'[{ScriptName}] Skipping...')

print(f'[{ScriptName}] Done.')



#%% [ERPs] Get 1s of raw data ==================================================
FigName = 'FigRawData'

Animals, ERPFolders, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{K}.dat')[0]
    for K in ('Animals', 'Folders', 'Treatments', 'Trials')
)


for A,Animal in enumerate(ExpInfo['Animals']):
    print(f"Processing animal {Animal} | {A+1} of {len(ExpInfo['Animals'])}...")

    ThisAnimal = Animals == Animal
    ThisTreats = Treatments[ThisAnimal]
    ThisTrials = Trials[ThisAnimal]
    ThisPlots = np.where(ThisAnimal)[0]

    N = np.ceil(len(ThisPlots)**0.5).astype(int)
    if len(ThisPlots) < 4: L, C = 1, len(ThisPlots)
    elif len(ThisPlots) == 4: L, C = 2, 2
    elif len(ThisPlots) in [5,6]: L, C = 2, 3
    elif len(ThisPlots) > 6: L, C = N

    Probe = ExpInfo['Probes'][ExpInfo['Animals']==Animal][0]

    if 'A4x4' in Probe:
        FS = np.array(Plot.FigSizeA4)*[1,0.5]
        Figs = [
            plt.figure(figsize=FS, constrained_layout=True)
            for _ in range(4)
        ]
        AxesL = [
            [Fig.add_subplot(L, C, _+1) for _ in range(len(ThisPlots))]
            for Fig in Figs
        ]

        ChMap = Analysis.RemapCh('A4x4-CM16', 'RHAOM') + [17]
        for p,P in enumerate(ThisPlots):
            Data, Rate = ERPs.Load(ERPFolders[P], ChMap)

            for S in range(4):
                Y = Data[:Rate,range(S*4, S*4+4)]
                Ax = AxesL[S][p]
                Plot.AllCh(Y, Ax=Ax)
                Ax.set_title(f'{ThisTreats[p]} trial {ThisTrials[p]}')
                Plot.Set(Ax)

        for F,Fig in enumerate(Figs):
            Fig.suptitle(f'Animal {Animal} Shank {F+1}')

            for E in Ext:
                Fig.savefig(f'{AnalysisGroup}/GroupData/SanityPlots/{FigName}_{Animal}_Shank{F+1}.{E}')

        plt.close('all')

    else:
        FS = np.array(Plot.FigSizeA4)

        Fig = plt.figure(figsize=FS, constrained_layout=True)
        Axes = [Fig.add_subplot(L, C, _+1) for _ in range(len(ThisPlots))]
        for A,Ax in enumerate(Axes):
            Data, Rate = ERPs.Load(ERPFolders[ThisPlots[A]])
            Plot.AllCh(Data[:Rate,:], Ax=Ax)
            Ax.set_title(f'{ThisTreats[A]} trial {ThisTrials[A]}')
            Plot.Set(Ax)
        Fig.suptitle(f'Animal {Animal}')

        for E in Ext:
            Fig.savefig(f'{AnalysisGroup}/GroupData/SanityPlots/{FigName}_{Animal}.{E}')

        plt.close('all')

print('All done.')



#%% [ERPs] Get ERP traces ======================================================
FilterFreq = (3, 60)
FilterOrder = 2
FilterType = 'bandpass'
ERPWindow = (-0.5, 1)

ERPFolders = IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/Folders.dat')[0]

print('Processing ERPs...')
for F,Folder in enumerate(ERPFolders):
    print(f'    Folder {F+1} of {len(ERPFolders)}...')

    if 'Baseline' in Folder: continue
    AnalysisFolder = Folder.replace(DataPath, AnalysisPath)
    Stim = Folder.split('_')[-1] if len(Folder.split('_')) == 3 else 'Unknown'
    if Stim not in TreatmentsOrder: continue

    Data, Rate = ERPs.Load(Folder)
    TTLs = ERPs.GetTTLs(Data[:,-1], Folder)
    ERP, X = ERPs.PairedERP(
        Data[:,:-1], Rate, TTLs,
        FilterFreq, FilterOrder, FilterType, ERPWindow
    )
    IO.Bin.Write({'ERPs':ERP,'ERPs_X':X}, AnalysisFolder)



#%% [ERPs] Get ERP channel =====================================================
Animals, ERPFolders = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{K}.dat')[0]
    for K in ('Animals', 'Folders')
)

Rate = 25000 # fixed for Intan RHA

DataChannels, DataChannelsMean, ERPMeans, ChosenCh, Rates = ([] for _ in range(5))
print('Processing ERPs...')
for F,Folder in enumerate(ERPFolders):
    print(f'    Folder {F+1} of {len(ERPFolders)}...')

    if 'Baseline' in Folder: continue
    AnalysisFolder = Folder.replace(DataPath, AnalysisPath)
    Stim = Folder.split('_')[-1] if len(Folder.split('_')) == 3 else 'Unknown'
    if Stim not in TreatmentsOrder: continue

    ERP, X = (
        IO.Bin.Read(f'{AnalysisFolder}/{_}.dat')[0]
        for _ in ('ERPs', 'ERPs_X')
    )

    FBrokenCh = ExpInfo['EPhys']['BrokenChs'][ExpInfo['Animals']==Animals[F]][0]
    ERPMean = ERP.mean(axis=1)

    Probe = ExpInfo['Probes'][ExpInfo['Animals']==Animals[F]][0]
    if Probe.split('-')[3] == '703':
        # Probes with large recording sites generate completely different
        # traces, with no phase reversal but variable N40 amplitude
        ChGood = [_ for _ in range(ERPMean.shape[1]) if _+1 not in FBrokenCh]
        Channel = ERPMean[(X>=0.03)*(X<=0.06),:][:,ChGood].min(axis=0).argmin()
        Channel = ChGood[Channel]+1
    else:
        Channel = ERPs.GetPhaseRevCh(ERPMean, X, FBrokenCh, [0,0.1])

    ChannelZP = Channel-1
    ChStart = ChannelZP-4 if ChannelZP>3 else 0

    Norm = Analysis.Normalize(ERPMean, (-1,1))
    if not len(DataChannels):
        DataChannels = np.zeros((Norm.shape[0], len(ERPFolders)), dtype=Norm.dtype)
        DataChannelsMean = np.zeros((Norm.shape[0], len(ERPFolders)), dtype=Norm.dtype)
        ERPMeans = np.zeros((ERPMean.shape[0], ERPMean.shape[1], len(ERPFolders)), dtype=ERPMean.dtype)
        ChosenCh = np.zeros(len(ERPFolders), dtype=int)
        Rates = np.zeros(len(ERPFolders), dtype=int)

    DataChannels[:,F] = Norm[:,ChannelZP]
    DataChannelsMean[:,F] = Norm[:,ChStart:ChannelZP+1].mean(axis=1)
    ERPMeans[:,:,F] = ERPMean
    ChosenCh[F] = Channel
    Rates[F] = Rate

ERPsTraces = {
    K: globals()[K] for K in (
        'DataChannels', 'DataChannelsMean', 'ERPMeans', 'Rates', 'X'
    )
}
ERPsTraces['Channels'] = ChosenCh
IO.Bin.Write(ERPsTraces, AnalysisGroup+'/GroupData/ERPsTraces')

print('Done.')



#%% [ERPs] Get ERP peaks =======================================================
DataChannels, X = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsTraces/{K}.dat')[0]
    for K in ('DataChannels', 'X')
)

Channels, ERPMeans, Rates, X = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsTraces/{K}.dat')[0]
    for K in ('Channels', 'ERPMeans', 'Rates', 'X')
)

Zeros = [np.where((X >= 0))[0][0], np.where((X >= 0.5))[0][0]]
BLStart = np.where((X >= -ExpInfo['EPhys']['BaselineDur']))[0][0]
BLStart = BLStart if BLStart > 0 else 0

PWindows = [
    [np.where((X>=P))[0][0] for P in W]
    for W in ExpInfo['EPhys']['PeakWindows']
]

ERPsDoublets = np.zeros((2, DataChannels.shape[1]), dtype=bool)
ERPsPeaks = np.zeros((2, DataChannels.shape[1], len(PeaksOrder)), dtype=int)

PeakAmps, PeakLats = (
    np.zeros((2, DataChannels.shape[1], len(PeaksOrder)), dtype=float)
    for _ in range(2)
)

print('Processing ERPs features...')
for DC in range(DataChannels.shape[1]):
    print(f'    Processing {DC+1} of {DataChannels.shape[1]}...')

    Norm = ERPMeans[:,Channels[DC]-1,DC]
    Rate = Rates[DC]
    BL = Norm[BLStart:Zeros[0]].mean()

    P20, N40, P80, Doublets = ERPs.GetComponents(Norm, X, PWindows)
    DCPeaks = dict(P20=P20,N40=N40,P80=P80)
    ERPsDoublets[:,DC] = Doublets

    for P,Peak in enumerate(PeaksOrder):
        ERPsPeaks[:,DC,P] = DCPeaks[Peak]
        PeakAmps[:,DC,P] = Norm[DCPeaks[Peak]]-BL

        PeakLats[:,DC,P] = [
            DCPeaks[Peak][0]/Rate+X[0],
            DCPeaks[Peak][1]/Rate+X[0]-0.5
        ]

PeakLats = PeakLats * 1000 # seconds to milliseconds

PeakAmpRatios, PeakLatRatios = (
    np.zeros((DataChannels.shape[1], len(PeaksOrder)), dtype=float)
    for _ in range(2)
)

for K,Key in enumerate(PeaksOrder):
    PeakAmpRatios[:,K], PeakLatRatios[:,K] = (
        (1 - (Set[1,:,K] / Set[0,:,K])) * 100
        for Set in (PeakAmps, PeakLats)
    )

PeakWidths = (
    ERPsPeaks[:,:,PeaksOrder.index('P80')] -
    ERPsPeaks[:,:,PeaksOrder.index('N40')]
)*1000/Rate

PeakWidthRatios = (1 - (PeakWidths[1,:] / PeakWidths[0,:])) * 100


IO.Bin.Write(
    {K: np.array(globals()[K]) for K in (
            'ERPsPeaks', 'ERPsDoublets', 'PeakAmps', 'PeakLats', 'PeakWidths',
            'PeakAmpRatios', 'PeakLatRatios', 'PeakWidthRatios', 'PeaksOrder'
    )},
    f'{AnalysisGroup}/GroupData/ERPsPeaks'
)

print('Done.')



#%% [ERPs] Sanity plots - Chosen channels ======================================
ScaleBar = 800
Normalize = True
FigName = f'FigChosenChannel'
FS = np.array(Plot.FigSizeA4)*[1,0.7]


Animals, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{K}.dat')[0]
    for K in ('Animals', 'Treatments', 'Trials')
)

Channels, ERPMeans, X = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsTraces/{K}.dat')[0]
    for K in ('Channels', 'ERPMeans', 'X')
)


for Trial in np.unique(Trials):
    if Trial > 0:
        AnimalsU = [
            Animal for Animal in np.unique(Animals)
            if max(Trials[Animals==Animal]) < Trial
        ]
        for Animal in AnimalsU:
            TrialMax = max(Trials[Animals==Animal])
            Trials[(Trials==TrialMax)*(Animals==Animal)] = Trial

    for A,Animal in enumerate(ExpInfo['Animals']):
        print(f"Processing animal {Animal} Trial {Trial+1} | {A+1} of {len(ExpInfo['Animals'])}...")
        ThisAnimal = Animals == Animal
        if True not in ThisAnimal: continue

        ThisTrial = Trials == Trial
        ThisTreats = Treatments[ThisAnimal*ThisTrial]
        ThisBrokenCh = np.array(ExpInfo['EPhys']['BrokenChs'][ExpInfo['Animals']==Animal][0])-1

        Fig, Axes = plt.subplots(1,len(ThisTreats), figsize=FS, constrained_layout=True, squeeze=False)
        for Treat in ThisTreats:
            ThisTreat = Treatments == Treat
            ThisPlot = ThisAnimal*ThisTreat*ThisTrial
            A = TreatmentsOrder.index(Treat)
            Y = ERPMeans[:,:,ThisPlot]

            if Y.shape[2] != 1:
                raise ValueError('There should be only one recording.')

            Y = Analysis.Normalize(Y[:,:,0], (-1,1)) if Normalize else Y[:,:,0]
            XL = (X>=-0.01)*(X<=0.2)
            SB = ScaleBar if not Normalize else 1
            ChosenCh = Channels[ThisPlot][0]-1

            ChLabels = (np.arange(Y.shape[1])+1).astype(str)

            if len(ThisBrokenCh):
                ChLabels[ThisBrokenCh] = ''

            Axes[0,A] = Plot.AllCh(
                Y[XL,:], X[XL], Labels=ChLabels, ScaleBar=SB, SpaceAmpF=1, lw=1,
                Ax=Axes[0,A]
            )

            for BC in ThisBrokenCh:
                Yp = Axes[0,A].get_lines()[BC+1].get_ydata()
                Axes[0,A].get_lines()[BC+1].set_ydata(np.ones(Yp.shape[0])*Yp.mean())

            SquareY = Axes[0,A].get_lines()[ChosenCh+1].get_ydata()
            SquareY = [SquareY.min(), SquareY.max(), SquareY.max(), SquareY.min(), SquareY.min()]
            SquareX = [X[XL][0], X[XL][0], X[XL][-1], X[XL][-1], X[XL][0]]
            Axes[0,A].plot(SquareX, SquareY, linestyle='--')

            Scale = Axes[0,A].get_lines()[0].get_xydata()
            Scale[:,0] -= 0.02
            Scale[:,1] -= 4700
            Axes[0,A].get_lines()[0].set_xdata(Scale[:,0])
            Axes[0,A].get_lines()[0].set_ydata(Scale[:,1])
            if not Normalize:
                Axes[0,A].text(
                    Scale[0,0]-0.03, np.mean(Scale[:,1]), f'{ScaleBar}µV',
                    rotation=90, ha='center', va='center', fontsize=8
                )

            Axes[0,A].set_xlabel('Time [s]')
            Axes[0,A].set_ylabel('Channels')
            Axes[0,A].set_title(Treat)
            Plot.Set(Ax=Axes[0,A])

        Fig.suptitle(f'Animal {Animal}')

        for E in Ext:
            Fig.savefig(f'{AnalysisGroup}/GroupData/SanityPlots/{FigName}_{Animal}_Trial{Trial}.{E}')

        plt.close('all')

print('All done.')



#%% [ERPs] Sanity plots - peak detection =======================================
FigName = f'FigPeakDetection'
MarkerPeak = '*'
FS = np.array(Plot.FigSizeA4)*[1,0.7]

Animals, Treatments, Trials = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{K}.dat')[0]
    for K in ('Animals', 'Treatments', 'Trials')
)

DataChannels, X = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsTraces/{K}.dat')[0]
    for K in ('DataChannels', 'X')
)

ERPsPeaks, Doublets = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsPeaks/{K}.dat')[0]
    for K in ('ERPsPeaks', 'ERPsDoublets')
)

Windows = ((X>=-0.01)*(X<=0.13), (X>=0.49)*(X<=0.63))

for Trial in np.unique(Trials):
    if Trial > 0:
        AnimalsU = [
            Animal for Animal in np.unique(Animals)
            if max(Trials[Animals==Animal]) < Trial
        ]
        for Animal in AnimalsU:
            TrialMax = max(Trials[Animals==Animal])
            Trials[(Trials==TrialMax)*(Animals==Animal)] = Trial

    for A,Animal in enumerate(ExpInfo['Animals']):
        print(f"Processing animal {Animal} Trial {Trial+1} | {A+1} of {len(ExpInfo['Animals'])}...")
        ThisAnimal = Animals == Animal
        if True not in ThisAnimal: continue

        ThisTrial = Trials == Trial
        ThisTreats = Treatments[ThisAnimal*ThisTrial]

        Fig, Axes = plt.subplots(len(ThisTreats), 2, figsize=FS, constrained_layout=True, squeeze=False)
        for Treat in ThisTreats:
            ThisTreat = Treatments == Treat
            ThisPlot = ThisAnimal*ThisTreat*ThisTrial
            A = TreatmentsOrder.index(Treat)

            Y = DataChannels[:,ThisPlot]

            if Y.shape[1] != 1:
                raise ValueError('There should be only one recording.')

            for PR in range(len(Windows)):
                Axes[A,PR].plot(X[Windows[PR]], Y[Windows[PR]], 'k', lw=1)
                Axes[A,PR].set_xticks(np.arange(0,0.14,0.02)+(0.5*PR))
                Axes[A,PR].set_xticklabels(np.arange(0,0.14,0.02))
                Axes[A,PR].set_xlabel('Time [s]')
                Axes[A,PR].set_ylabel('Channels')
                Axes[A,PR].set_title(
                    f'{Treatments[ThisPlot][0]} Click {PR+1} | is doublet: {Doublets[PR,ThisPlot]}'
                )

            for P,Peak in enumerate(PeaksOrder):
                YP = ERPsPeaks[:,ThisPlot,P]
                for PR,PeakRep in enumerate(YP):
                    Label = Peak if not A and not PR else None
                    Axes[A,PR].plot(X[PeakRep], Y[PeakRep], MarkerPeak, label=Label)

                    if not A and not PR: Axes[A,PR].legend()
                    Plot.Set(Ax=Axes[A,PR])

        Fig.suptitle(f'Animal {Animal} Trial {Trial}')

        for E in Ext:
            Fig.savefig(f'{AnalysisGroup}/GroupData/SanityPlots/{FigName}_{Animal}_Trial{Trial}.{E}')

        plt.close('all')

print('All done.')



#%% [ERPs] Stats ===============================================================
FacOrder = ('Click', 'Genotype', 'Treatment')
FacPaired = (True, False, True)
FacOrderRatio = ('Genotype','Treatment')
FacPairedRatio = (False, True)

AOut = f'{AnalysisGroup}/GroupData/Stats'

Animals, Treatments, Genotypes, Trials = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/GroupInfo/{_}.dat')[0]
    for _ in ('Animals', 'Treatments', 'Genotypes', 'Trials')
)

Keys, PeakAmps, PeakLats, PeakWidths = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsPeaks/{_}.dat')[0]
    for _ in ('PeaksOrder', 'PeakAmps', 'PeakLats', 'PeakWidths')
)

PeakAmpRatios, PeakLatRatios, PeakWidthRatios = (
    IO.Bin.Read(f'{AnalysisGroup}/GroupData/ERPsPeaks/{_}.dat')[0]
    for _ in ('PeakAmpRatios', 'PeakLatRatios', 'PeakWidthRatios')
)


for Trial in np.unique(Trials):
    if Trial > 0:
        AnimalsU = [
            Animal for Animal in np.unique(Animals)
            if max(Trials[Animals==Animal]) < Trial
        ]
        for Animal in AnimalsU:
            TrialMax = max(Trials[Animals==Animal])
            Trials[(Trials==TrialMax)*(Animals==Animal)] = Trial


    print(f'[{ScriptName}] Processing Amps and Lats...')
    aftr = np.tile(Trials,2)
    aftr = aftr == Trial

    FXs = {
        'Click': np.array((['1']*Genotypes.shape[0])+(['2']*Genotypes.shape[0])),
        'Genotype': np.tile(Genotypes,2),
        'Treatment': np.tile(Treatments,2)
    }
    FXs = {K: V[aftr] for K,V in FXs.items()}
    afs = [FXs[_] for _ in FacOrder]
    aid = np.tile(Animals,2)[aftr]

    for K,Key in enumerate(Keys):
        for D,Data in (('Amp', PeakAmps),('Lat',PeakLats)):
            print(f'[{ScriptName}]     Trial {Trial} {Key} {D}...')
            ad = Data[:,:,K].ravel()[aftr]

            A = Stats.AnOVa(ad, afs, aid, FacPaired, 'auto', FacOrder)
            for _ in Stats.GetAnovaReport(A,FacOrder): print(_)

            IO.Txt.Write(A, f"{AOut}/ERPs-{D}-{'_'.join(FacOrder)}-{Key}-Trial{Trial}.dict")
            IO.Bin.Write(
                dict(
                    Data=ad,
                    FacNames=np.array(FacOrder),
                    FacPaired=np.array(FacPaired),
                    Ids=aid,
                    FXs=np.array(afs).T
                ),
                f"{AOut}/ERPs-{D}-{'_'.join(FacOrder)}-{Key}-Trial{Trial}_Data"
            )


    print(f'[{ScriptName}]     Trial {Trial} Widths...')
    ad = PeakWidths.ravel()[aftr]
    A = Stats.AnOVa(ad, afs, aid, FacPaired, 'auto', FacOrder)
    for _ in Stats.GetAnovaReport(A,FacOrder): print(_)

    IO.Txt.Write(A, f"{AnalysisGroup}/GroupData/Stats/ERPs-Width-{'_'.join(FacOrder)}-Trial{Trial}.dict")
    IO.Bin.Write(
        dict(
            Data=ad,
            FacNames=np.array(FacOrder),
            FacPaired=np.array(FacPaired),
            Ids=aid,
            FXs=np.array(afs).T
        ),
        f"{AnalysisGroup}/GroupData/Stats/ERPs-Width-{'_'.join(FacOrder)}-Trial{Trial}_Data"
    )


    print(f'[{ScriptName}] Processing Ratios...')
    aftr = Trials == Trial
    FXs = {'Genotype': Genotypes, 'Treatment': Treatments}
    FXs = {K: V[aftr] for K,V in FXs.items()}
    afs = [FXs[_] for _ in FacOrderRatio]
    aid = Animals[aftr]

    for K,Key in enumerate(Keys):
        for D,Data in (('Amp', PeakAmpRatios),('Lat',PeakLatRatios)):
            print(f'[{ScriptName}]     Trial {Trial} {Key} {D}...')
            ad = Data[:,K][aftr]

            A = Stats.AnOVa(ad, afs, aid, FacPairedRatio, 'auto', FacOrderRatio)
            for _ in Stats.GetAnovaReport(A,FacOrderRatio): print(_)

            IO.Txt.Write(A, f"{AnalysisGroup}/GroupData/Stats/ERPs-Ratio{D}-{'_'.join(FacOrderRatio)}-{Key}-Trial{Trial}.dict")
            IO.Bin.Write(
                dict(
                    Data=ad,
                    FacNames=np.array(FacOrderRatio),
                    FacPaired=np.array(FacPairedRatio),
                    Ids=aid,
                    FXs=np.array(afs).T
                ),
                f"{AnalysisGroup}/GroupData/Stats/ERPs-Ratio{D}-{'_'.join(FacOrderRatio)}-{Key}-Trial{Trial}_Data"
            )


    print(f'[{ScriptName}]     Trial {Trial} Width ratios...')
    ad = PeakWidthRatios[aftr]
    A = Stats.AnOVa(ad, afs, aid, FacPairedRatio, 'auto', FacOrderRatio)
    for _ in Stats.GetAnovaReport(A,FacOrderRatio): print(_)

    IO.Txt.Write(A, f"{AnalysisGroup}/GroupData/Stats/ERPs-RatioWidth-{'_'.join(FacOrderRatio)}-Trial{Trial}.dict")
    IO.Bin.Write(
        dict(
            Data=ad,
            FacNames=np.array(FacOrderRatio),
            FacPaired=np.array(FacPairedRatio),
            Ids=aid,
            FXs=np.array(afs).T
        ),
        f"{AnalysisGroup}/GroupData/Stats/ERPs-RatioWidth-{'_'.join(FacOrderRatio)}-Trial{Trial}_Data"
    )


print(f'[{ScriptName}] Done.')



#%% EOF ========================================================================
